obj-m := 4d-hats.o
obj-m += spi-rockchip-test.o
obj-m += spi-rockchip.o
obj-m += ar1021_i2c.o
obj-m += edt-ft5x06.o

all:
	$(MAKE) ARCH=arm -C /root/debian_kernel  M=$(PWD) modules

clean:
	$(MAKE) ARCH=arm -C /root/debian_kernel M=$(PWD) clean

